/**
 *  Copyright (C) 2012  plegal  (plegal@appert44.org)
 *  @file         Nain.cpp
 *  @brief        Classe Nain
 *  @version      0.1
 *  @date         26 nov. 2012 10:47:12
 *
 *  Description detaillee du fichier Nain.cpp
 *  Fabrication   gcc (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3 
 *  @todo         Liste des choses restant a faire.
 *  @bug          26 nov. 2012 10:47:12 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
#include <iostream>
using namespace std;

// Includes qt

// Includes application
#include "Nain.h"

/**
 * Constructeur
 */
Nain::Nain(int numero, QString aller, QString retour)
{
    fin_ = false;
    numero_ = numero;
    aller_ = aller;
    retour_ = retour;
    semPlein_ = NULL;
    semVide_ = NULL;
    coffret_ = NULL;
}

/**
 * Destructeur
 */
Nain::~Nain()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Nain::NomMethode(Type parametre)
// {
// }

// Methodes protegees
void Nain::run()
{
    int diamant;
    while (!fin_)
    {
        cout << "Nain " << numero_ <<"  : je pioche..." << endl;
        sleep(rand()%4);
        diamant = rand()%10+1; //carat
        cout << "Nain " << numero_ <<"  : j'ai trouvé un diamant de " << diamant << " carats ! " << endl;
        semVide_->acquire();
        coffret_->push_back(diamant);
        cout << "Nain " << numero_ <<"  : Envoit du diamant (" << diamant << ") dans le coffre !" << endl;
        cout << "Nain " << numero_ <<"  : il y a maintenant " << coffret_->size() << " diamant de "<< diamant << " carats dans le coffret  ! " << endl;
        semPlein_->release();
    }
}
// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Nain" << endl;
//    Nain *nain = new Nain();
//    // Lancement de l'activité principale
//    return 0;
// }

