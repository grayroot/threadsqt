/**
 *  Copyright (C) 2012  plegal  (plegal@appert44.org)
 *  @file         Nain.h
 *  @brief        Classe Nain
 *  @version      0.1
 *  @date         26 nov. 2012 10:47:12
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Nain.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _NAIN_H
#define _NAIN_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++

// Includes qt
#include <QThread>
#include <QSemaphore>
#include <QVector>
// Includes application

// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Classe Nain
 *  Description detaillee de la classe.
 */
class Nain: public QThread
{
public :
    /**
     * Constructeur
     */
    Nain(int numero, QString aller, QString retour);
    /**
     * Destructeur
     */
    ~Nain();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }
    void setFin(bool fin) { fin_ = fin; }
    void setSemPlein(QSemaphore *sem) {semPlein_ = sem;}
    void setSemVide(QSemaphore *sem) {semVide_ = sem; }
    void setCoffret(QVector<int> *coffret) {coffret_ = coffret;}
protected :
    // Attributs proteges

    // Methode protegees
    void run ();
private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;
    bool fin_;
    QSemaphore *semPlein_;
    QSemaphore *semVide_;
	QVector<int> *coffret_;
    int numero_;
    QString aller_;
    QString retour_;
    // Methodes privees
};

// Methodes publiques inline
// ex :
// inline void Nain::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Nain::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _NAIN_H

