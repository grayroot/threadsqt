/**
 *  Copyright (C) 2012  plegal  (plegal@appert44.org)
 *  @file         BlancheNeige.cpp
 *  @brief        Classe BlancheNeige
 *  @version      0.1
 *  @date         26 nov. 2012 10:24:15
 *
 *  Description detaillee du fichier BlancheNeige.cpp
 *  Fabrication   gcc (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3 
 *  @todo         Liste des choses restant a faire.
 *  @bug          26 nov. 2012 10:24:15 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
#include <iostream>
using namespace std;

// Includes qt

// Includes application
#include "BlancheNeige.h"

/**
 * Constructeur
 */
BlancheNeige::BlancheNeige()
{
	semPlein_ = NULL;
	semVide_ = NULL;
	coffret_ = NULL;
	fin_ = false;
}

/**
 * Destructeur
 */
BlancheNeige::~BlancheNeige()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType BlancheNeige::NomMethode(Type parametre)
// {
// }

// Methodes protegees
void BlancheNeige::run()
{
	cout << ".......Blanche Neige : En cours" << endl;
	    while (!fin_)
	    {
	        cout << "Blanche Neige : Une petite sieste me fera du bien..." << endl;
	        sleep(rand()%10);
	        semPlein_->acquire();
	        cout << "Blanche Neige : Si je vidais le coffret ?" << endl;
	        coffret_->back();
	        cout << "Blanche Neige : Il y a "<< coffret_->size() << " diamants dans le coffret" << endl;
	        cout << "Blanche Neige : Et hop ! un diamant de " << coffret_->back() << " carats pour moi" << endl;
	        coffret_->clear();
	        semVide_->release();
	    }
	    cout << "Blanche Neige : Terminé" << endl;
}
// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale BlancheNeige" << endl;
//    BlancheNeige *blancheneige = new BlancheNeige();
//    // Lancement de l'activité principale
//    return 0;
// }

