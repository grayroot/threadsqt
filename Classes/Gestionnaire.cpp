/**
 *  Copyright (C) 2012  plegal  (plegal@appert44.org)
 *  @file         Gestionnaire.cpp
 *  @brief        Classe Principale - TD Multithreading
 *  @version      0.1
 *  @date         26 nov. 2012 08:29:39
 *
 *  Description detaillee du fichier Gestionnaire.cpp
 *  Fabrication   gcc (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3 
 *  @todo         Liste des choses restant a faire.
 *  @bug          26 nov. 2012 08:29:39 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
// Includes system C++
// A décommenter si besoin cout, cin, ...
#include <iostream>
using namespace std;

// Includes qt

// Includes application
#include "Gestionnaire.h"

/**
 * Constructeur
 */
Gestionnaire::Gestionnaire()
{
    blancheneige_ = new BlancheNeige();

    nains_[0] = new Nain(1,"Haïli","Haïli");

    coffret_ = new QVector<int>;
    semPlein_ = new QSemaphore(0);
    semVide_ = new QSemaphore(1);

    // Mise en place des associations
    nains_[0]->setSemPlein(semPlein_);
    nains_[0]->setSemVide(semVide_);
    nains_[0]->setCoffret(coffret_);
    blancheneige_->setSemPlein(semPlein_);
    blancheneige_->setSemVide(semVide_);
    blancheneige_->setCoffret(coffret_);

    srand(time(NULL)); //initialisation du générateur aléatoire
}

/**
 * Destructeur
 */
Gestionnaire::~Gestionnaire()
{
    delete blancheneige_;
    delete nains_[0];
    delete coffret_;
    delete semPlein_;
    delete semVide_;
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
void Gestionnaire::BlancheNeigeLeNainEtLeCoffretDeDiamant()
{
    cout << "Thread main crée et en cours" << endl;
    cout << "Voix off : nain et blancheNeige crées" << endl;
    nains_[0]->start();
    blancheneige_->start();
    sleep(60);
    nains_[0]->terminate();
    blancheneige_->terminate();
    cout << "Thread main terminé" << endl;
}

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
int main(int argc, char *argv[])
{
   // Construction de l'instance de la classe principale
   cout << "Construction de l'instance de la classe principale Gestionnaire" << endl;
   Gestionnaire *gestionnaire = new Gestionnaire();
   // Lancement de l'activité principale
   gestionnaire->BlancheNeigeLeNainEtLeCoffretDeDiamant();
   delete gestionnaire;
   return 0;
}

